google.charts.load("current", { packages: ["timeline"] });
google.charts.setOnLoadCallback(drawChart);
var today = new Date();

function date(day, month, year) {
  return new Date(year, month - 1, day);
}

function start(day, month, year) {
  return date(day, month, year);
}

function end(day, month, year) {
  var d = date(day, month, year);
  d.setDate(d.getDate() + 1);
  return d;
}

function drawChart() {
  var container = document.getElementById("timeline-container");
  var chart = new google.visualization.Timeline(container);
  var dataTable = new google.visualization.DataTable();
  dataTable.addColumn({ type: "string", id: "Module" });
  dataTable.addColumn({ type: "string", id: "Project" });
  dataTable.addColumn({ type: "date", id: "Start" });
  dataTable.addColumn({ type: "date", id: "End" });
  var now = new Date(today.getFullYear(), today.getMonth(), today.getDate());
  dataTable.addRows([
    ['\0', 'Now', now, now],
    ['B2 - Binary Security', 'Call For Papers', start(13, 04, 2020), start(29, 04, 2020)],
    ['B2 - Binary Security', 'NOOB', start(04, 05, 2020), start(17, 05, 2020)],
    ['B2 - C Graphical Programming', 'MyDefender', start(17, 02, 2020), start(15, 03, 2020)],
    ['B2 - C Graphical Programming', 'MyWorld', start(17, 02, 2020), start(15, 03, 2020)],
    ['B2 - C Graphical Programming', 'MyRPG', start(16, 03, 2020), start(03, 05, 2020)],
    ['B2 - C Graphical Programming', 'Back To The Future - MyWorld', start(13, 04, 2020), start(19, 04, 2020)],
    ['B2 - C Graphical Programming', 'Back To The Future - MyDefender', start(13, 04, 2020), start(19, 04, 2020)],
    ['B2 - C Graphical Programming', 'Back To The Future - MyWorld', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - C Graphical Programming', 'Back To The Future - MyDefender', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Elementary programming in C (Part I)', 'Dante\'s star', start(02, 03, 2020), start(22, 03, 2020)],
    ['B2 - Elementary programming in C (Part I)', 'Lem-in', start(23, 03, 2020), start(19, 04, 2020)],
    ['B2 - Elementary programming in C (Part I)', 'Back To The Future - Dante\'s star', start(13, 04, 2020), start(19, 04, 2020)],
    ['B2 - Elementary programming in C (Part I)', 'Back To The Future - Dante\'s star', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Elementary programming in C (Part I)', 'Back To The Future - Lem-in', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Elementary programming in C (Part II)', 'Corewar', start(20, 04, 2020), start(17, 05, 2020)],
    ['B2 - FR - Écrits professionnels', '"Le mode d\'emploi : prendre le lecteur par la main"', start(27, 01, 2020), start(16, 02, 2020)],
    ['B2 - FR - Écrits professionnels', '"Faire préciser"', start(24, 02, 2020), start(08, 03, 2020)],
    ['B2 - FR - Écrits professionnels', '"La lettre de vente : informer, argumenter, valoriser"', start(16, 03, 2020), start(12, 04, 2020)],
    ['B2 - Introduction to A.I.', 'Need4Stek', start(06, 04, 2020), start(10, 05, 2020)],
    ['B2 - Introduction to A.I.', 'Back To The Future - Need4Stek', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Introduction to Web Development', 'EPyTodo', start(16, 03, 2020), start(12, 04, 2020)],
    ['B2 - Introduction to Web Development', 'Back To The Future - EPyTodo', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Mathematics', '106bombyx', start(17, 02, 2020), start(01, 03, 2020)],
    ['B2 - Mathematics', '107transfer', start(02, 03, 2020), start(15, 03, 2020)],
    ['B2 - Mathematics', '108trigo', start(16, 03, 2020), start(29, 03, 2020)],
    ['B2 - Mathematics', '109titration', start(30, 03, 2020), start(12, 04, 2020)],
    ['B2 - Mathematics', '110borwein', start(13, 04, 2020), start(26, 04, 2020)],
    ['B2 - Networks and Systems Administration', 'MyWeb', start(24, 02, 2020), start(22, 03, 2020)],
    ['B2 - Shell Programming', 'MiniShell 2', start(30, 03, 2020), start(26, 04, 2020)],
    ['B2 - Shell Programming', '42sh', start(27, 04, 2020), start(24, 05, 2020)],
    ['B2 - Shell Programming', 'Back To The Future - MiniShell 2', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Unix System programming', 'Navy', start(17, 02, 2020), start(08, 03, 2020)],
    ['B2 - Unix System programming', 'Tetris', start(09, 03, 2020), start(29, 03, 2020)],
    ['B2 - Unix System programming', 'Back To The Future - Navy', start(13, 04, 2020), start(19, 04, 2020)],
    ['B2 - Unix System programming', 'Back To The Future - Tetris', start(13, 04, 2020), start(19, 04, 2020)],
    ['B2 - Unix System programming', 'Back To The Future - Navy', start(25, 05, 2020), start(31, 05, 2020)],
    ['B2 - Unix System programming', 'Back To The Future - Tetris', start(25, 05, 2020), start(31, 05, 2020)],
  ]);

  chart.draw(dataTable, {
    timeline: {
      colorByRowLabel: true
    }
  });

  nowLine("timeline-container");

  google.visualization.events.addListener(chart, "onmouseover", function(obj) {
    if (obj.row == 0) {
      $(".google-visualization-tooltip").css("display", "none");
    }
    nowLine("timeline-container");
  });

  google.visualization.events.addListener(chart, "onmouseout", function(obj) {
    nowLine("timeline-container");
  });
}

function nowLine(div) {
  //get the height of the timeline div
  var height;
  $("#" + div + " rect").each(function(index) {
    var x = parseFloat($(this).attr("x"));
    var y = parseFloat($(this).attr("y"));

    if (x == 0 && y == 0) {
      height = parseFloat($(this).attr("height"));
    }
  });

  var nowWord = $("#" + div + ' text:contains("Now")');

  nowWord
    .prev()
    .first()
    .attr("height", height + "px")
    .attr("width", "1px")
    .attr("y", "0");
  // add this line to remove the display:none style on the vertical line
  $("#" + div + '  text:contains("Now")').each(function(idx, value) {
    if (idx == 0) {
      $(value)
        .parent()
        .find("rect")
        .first()
        .removeAttr("style");
    } else if (idx == 1) {
      $(value)
        .parent()
        .find("rect")
        .first()
        .attr("style", "display:none;");
    }
  });
}

var repourl = "https://gitlab.com/epi-codes/Epitech-2024-Timeline";
$(document).ready(function() {
  $.getJSON(
    "https://gitlab.com/api/v4/projects/epi-codes%2fEpitech-2024-Timeline/repository/commits",
    function(json) {
      var msg, el, date;

      $("#changelog-container").empty();

      console.log(json);

      for (var i = 0; i < json.length; i++) {
        msg = json[i].message.split("\n");
        date = moment(json[i].created_at);
        el = $(`<p class="commit">
<a href="${repourl}/commit/${json[i].id}" target="_blank" class="commit-msg">${
          msg[0]
        }</a>
<span title="${date.format(
          "dddd, MMMM Do YYYY, h:mm:ss a"
        )}" class="commit-date">${date.fromNow()}</span>
</p>`);
        if (msg.length > 1) {
          for (var j = msg.length - 1; j >= 1; j--) {
            if (msg[j].length > 0) {
              el.addClass("expanded");
              el.find("a").after(`<span class="commit-desc">${msg[j]}</span>`);
            }
          }
        }
        el.appendTo($("#changelog-container"));
      }

      if (json.length <= 0) {
        $("#changelog-container").text("No commits !? xO");
      }
    }
  ).fail(function() {
    $("#changelog-container").text("Error while loading changelog :'(");
  });

  function set_theme(dark) {
    var dark = dark || false;

    window.localStorage.setItem("dark", dark);

    if (dark) {
      $("body").addClass("dark");
      $("#switch").text("Switch to light");
    } else {
      $("body").removeClass("dark");
      $("#switch").text("Switch to dark");
    }
  }

  $("#switch").on("click", function() {
    set_theme(!$("body").hasClass("dark"));
    return false;
  });

  set_theme(window.localStorage.getItem("dark") == "true" ? true : false);
  setTimeout(function() {
    $("body").addClass("ready");
  }, 500);
});
